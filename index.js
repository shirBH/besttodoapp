const $ = (s, p = document) => p.querySelector(s);
const $$ = (s, p = document) => p.querySelectorAll(s);
let toDoList = window.localStorage.getItem("toDo")
  ? JSON.parse(window.localStorage.getItem("toDo"))
  : {};
//function to add a todo item
const addItem = (str) => {
  let toAdd = `<li>
    <input class="checkbox" type="checkbox" >
    <label class="todoString">${str}</label>
    <input type="text">
    <button class="delete">X</button>
  </li>`;
  toDoList[str] = false;
  window.localStorage.setItem("toDo", JSON.stringify(toDoList));
  document.querySelector(".tasks").innerHTML += toAdd;
  //checkbox mark unmark
  let checkboxes = $$(".checkbox");
  for (const checkbox of checkboxes) {
    checkbox.addEventListener("change", (e) => {
      e.path[1].children[1].classList.toggle("line");
      toDoList[e.path[1].children[1].innerText] = !toDoList[
        e.path[1].children[1].innerText
      ];
    });
  }
  //delete buttons
  let deleteBtns = $$(".delete");
  for (const btn of deleteBtns) {
    btn.addEventListener("click", (e) => {
      let prop = e.path[1].innerText.split(/\s+/)[0];
      delete toDoList[prop];
      window.localStorage.setItem("toDo", JSON.stringify(toDoList));
      e.path[1].remove();
    });
  }
};
//filter only non checked items
const showDone = () => {
  let items = $$("li");
  for (let item of items) {
    if (toDoList[item.innerText] === false) {
      console.log("here");
      item.classList.add("hide");
    } else {
      item.classList.remove("hide");
    }
  }
};
//show all items
const showAll = () => {
  let items = $$("li");
  for (let item of items) {
    item.classList.remove("hide");
  }
};
//show un done items
const showUnDone = () => {
  let items = $$("li");
  for (let item of items) {
    if (toDoList[item.innerText] === true) {
      item.classList.add("hide");
    } else {
      item.classList.remove("hide");
    }
  }
};

const deleteAllChecked = () => {
  let lis = $$("li");
  for (const item of lis) {
    if (item.childNodes[1].checked === true) {
      let prop = item.innerText.split(/\s+/)[0];
      delete toDoList[prop];
      window.localStorage.setItem("toDo", JSON.stringify(toDoList));
      item.remove();
    }
  }
};
//render initial items
const renderItems = () => {
  $(".tasks").innerText = "";
  for (let key of Object.keys(toDoList)) {
    addItem(key);
  }
};
const init = () => {
  renderItems();
  $(".addItemBtn").addEventListener("click", () => {
    let str = $(".inputText").value;
    addItem(str);
  });
  $(".deleteAllChecked").addEventListener("click", () => {
    deleteAllChecked();
  });
  $(".done").addEventListener("click", () => {
    showDone();
  });
  $(".all").addEventListener("click", () => {
    showAll();
  });
  $(".undone").addEventListener("click", () => {
    showUnDone();
  });
};

init();
